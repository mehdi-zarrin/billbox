<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class OrderItemsMigration_100
 */
class OrderItemsMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('order_items', [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'ordersId',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'productsId',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'after' => 'ordersId'
                        ]
                    ),
                    new Column(
                        'qty',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'after' => 'productsId'
                        ]
                    ),
                    new Column(
                        'total',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'after' => 'qty'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('order_items_pkey', ['id'], 'PRIMARY KEY')
                ],

            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
