<?php


class OrderDetailsController extends ControllerBase
{
    public function showAction($id)
    {
        $this->view->order = $this->orderRepository->findById($id);
    }
}