<?php

use Phalcon\Mvc\Controller;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class ControllerBase extends Controller
{

    public function paginate($data, $limit = 4)
    {
        $currentPage = $this->request->getQuery('page', null, false);
        $paginator = new PaginatorModel(
            [
                'data'  => $data,
                'limit' => $limit,
                'page'  => $currentPage,
            ]
        );
        return $paginator->paginate();
    }

}
