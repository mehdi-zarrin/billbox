<?php

class ShoppingCartController extends ControllerBase
{
    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function indexAction()
    {
        $this->view->cartItems = $this->cart->getItems();
    }

    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function addAction($id)
    {
        $product = Products::findFirstById((int) $id);

        if($product) {
            $this->cart->add($product->toArray());
        }

        $this->response->redirect('shopping-cart');
    }

    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function removeAction($id)
    {
        $this->cart->removeItem($id);
        $this->response->redirect('shopping-cart');
    }


}