<?php

use Billbox\Exceptions\AuthenticationException;
use Billbox\Exceptions\ValidationException;
use Billbox\Forms\LoginForm;

class SessionsController extends ControllerBase
{
    public function loginAction()
    {
        $form = new LoginForm();

        try {

            if($this->request->isPost()) {

                if($form->isValid($this->request->getPost())) {

                    $this->auth->attempt($this->request->getPost());
                    $this->response->redirect('orders');

                }
            }

        } catch(AuthenticationException $e) {

            $this->flash->error($e->getMessage());

        }

        $this->view->form = $form;
    }

    public function registerAction()
    {

        $form = new \Billbox\Forms\RegisterForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) != false) {

                $attributes = $this->request->getPost();
                $attributes['password'] = $this->security->hash($attributes['password']);
                $user = false;

                try {

                    $user = $this->userRepository->save($attributes);

                } catch (ValidationException $e) {

                    $this->flash->error($this->userRepository->getMessages());

                }

                if($user) {

                    $this->auth->login($user);
                    $this->response->redirect('/');
                }


            }
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->auth->remove();
        $this->response->redirect('sessions/login');
    }

}