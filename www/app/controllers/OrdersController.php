<?php

use Billbox\Exceptions\ValidationException;

class OrdersController extends ControllerBase
{
    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function indexAction() 
    {
        $user = $this->userRepository->findById(
            $this->auth->get('id')
        );

        $this->view->page = $this->paginate($user->latestOrders);
    }

    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function deliveredAction()
    {
        $user = $this->userRepository->findById(
            $this->auth->get('id')
        );

        $this->view->page = $this->paginate($user->deliveredOrders);
        $this->view->setContent(
            $this->view->render('orders', 'index')
        );
    }

    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function createAction()
    {
        try {

            $this->orderRepository->saveWithItems(
                $this->cart->getItems(),
                $this->auth->getUser()
            );

            $this->cart->clearCart();

        } catch( ValidationException $e) {

            $this->flash->error('Something went wrong! Please order later!');
        }

        $this->response->redirect('orders');
    }

    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function removeAction($id)
    {
        $this->orderRepository->remove($id);
        $this->response->redirect('orders');
    }

    
}