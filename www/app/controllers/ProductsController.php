<?php

class ProductsController extends ControllerBase
{
    /**
     * @AuthMiddleware("Billbox\Middlewares\MustbeLoggedIn")
     */
    public function indexAction()
    {
        $data = Products::find();
        $this->view->page = $this->paginate($data);
    }
}

