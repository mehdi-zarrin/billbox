<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
    ]
)->register();

$loader->registerNamespaces(
    [
        'Billbox' => APP_PATH . '/Billbox/'
    ]
)->register();

// require composer autoloader to include outside of the framework dependencies
require_once BASE_PATH . '/vendor/autoload.php';
