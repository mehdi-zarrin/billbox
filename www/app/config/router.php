<?php

$router = $di->getRouter();

// Define your routes here
$router->add(
    '/orders',
    [
        'controller' => 'orders',
        'action' => 'index'
    ]
)->setName('orders');

$router->add(
    '/orders/delivered',
    [
        'controller' => 'orders',
        'action' => 'delivered'
    ]
)->setName('deliveredOrders');

$router->add(
    '/',
    [
        'controller' => 'products',
        'action' => 'index'
    ]
);

$router->add(
    '/shopping-cart',
    [
        'controller' => 'shopping-cart',
        'action' => 'index'
    ]
)->setName('shoppingCart');

$router->handle();
