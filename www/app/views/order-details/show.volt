<div class="card">
    <div class="card-header">
        Order details
    </div>

    <div class="card-body">
        {% if order %}
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
            {% for item in order.items %}
            <tr>
                <td>{{ item.product.name }}</td>
                <td>{{ item.product.price }}</td>
                <td>{{ item.qty }}</td>
                <td>{{ item.total }}</td>
            </tr>
            {% endfor %}
        </table>
        <a href="/orders" class="btn btn-primary btn-sm"> Back to orders</a>
        {% else %}
            <div class="alert alert-warning">Nothing to show</div>
        {% endif %}
    </div>


</div>

