<div class="content mt-5">
    <div class="card">
        <div class="card-header">Shopping cart Items</div>
        <div class="card-body">
            {% if cartItems | length %}
            <table class="table">
                <tr>
                    <th>Product Name</th>
                    <th>Product Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th>Remove</th>
                </tr>
                {% for id, item in cartItems %}
                    <tr>
                        <td>{{ item['item']['name'] }}</td>
                        <td>{{ priceTag.formatPrice(item['item']['price']) }}</td>
                        <td>{{ item['qty'] }}</td>
                        <td>{{ priceTag.formatPrice(item['total']) }}</td>
                        <td>
                            <a href="/shopping-cart/remove/{{ id }}" class="btn btn-danger btn-sm">Remove</a>
                        </td>
                    </tr>
                {% endfor %}
            </table>
            <a href="/orders/create" class="btn btn-primary">Order</a>
            {% else %}
            <h4 class="alert alert-warning">
                There is not item in your cart
                <br />
                <a href="/" class="btn btn-sm btn-primary mt-3">Go shopping</a>
            </h4>
            {% endif %}
        </div>
    </div>

</div>