<div class="row mt-5">
    {% for item in page.items %}
    <div class="col-md-3 mt-3 mb-3">
        <div class="card">
            <img class="card-img-top" src="https://fakeimg.pl/300/?text= {{ item.name }}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{ item.name }}</h5>
                <p class="card-text">{{ item.price }}</p>
                <a href="/shopping-cart/add/{{ item.id }}" class="btn btn-primary">Add to cart</a>
            </div>
        </div>
    </div>
    {% endfor %}
</div>
{{ partial('partials/pagination') }}