<div class="card mt-5">
    <div class="card-header">
        Orders
    </div>
    <div class="card-body">

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link {{ router.getMatchedRoute().getName() == 'orders' ? 'active' : ''}}" href="/orders">Latest</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ router.getMatchedRoute().getName() == 'deliveredOrders' ? 'active' : ''}}" href="/orders/delivered">Delivered</a>
            </li>
        </ul>

        {% if page.items|length > 0 %}

        <table class="table" style="border-top: none">
            <tr>
                <th>Reference Number</th>
                <th>Total</th>
                <th>Created At</th>
                <th>Status</th>
                <th>Remove</th>
                <th>Details</th>
            </tr>
            {% for order in page.items %}
                <tr>
                    <td>{{ order.ref }}</td>
                    <td>{{ order.total }}</td>
                    <td>{{ order.created_at }}</td>
                    <td>
                        {{ order.delivered ?
                        '<div class="text-success"> Delivered</div>' :
                        '<div class="text-warning"> Pending</div>'
                        }}
                    </td>
                    <td>
                        <a href="/orders/remove/{{ order.id }}" class="btn btn-danger btn-sm">Remove</a>
                    </td>
                    <td>
                        <a href="/order-details/show/{{ order.id }}" class="btn btn-primary btn-sm">Details</a>
                    </td>
                </tr>
            {% endfor %}
        </table>
        {% else %}
            <div class="alert alert-warning mt-3">You don't have any orders at the moment</div>
        {% endif %}
        {{ partial('partials/pagination') }}
    </div>
</div>
