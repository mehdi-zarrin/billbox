{{ form() }}
<div class="card mt-5">
    <div class="card-header">
        Login
    </div>
    <div class="card-body justify-content-center">
        {% if(flash.output() | length) %}
            {{ flash.outPut() }}
        {% endif %}
        <div class="form-group">
            <label>Email</label>
            {{ form.render('email', ['class': 'form-control']) }}
            {{ form.messages('email')}}
        </div>

        <div class="form-group">
            <label>Password</label>
            {{ form.render('password', ['class': 'form-control']) }}
            {{ form.messages('password')}}
        </div>

        {{ form.render('csrf', ['value': security.getToken()]) }}
        {{ form.render('Login') }}
    </div>
</div>
