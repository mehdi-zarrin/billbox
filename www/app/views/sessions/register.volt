{{ content() }}

<div class="card mt-5">
    <div class="card-header">Register</div>
    <div class="card-body">
        {{ form() }}
        <div class="form-group">
            <label>First Name</label>
            {{ form.render('firstName', ['class': 'form-control']) }}
        </div>

        <div class="form-group">
            <label>LastName</label>
            {{ form.render('lastName', ['class': 'form-control']) }}
        </div>
        
        <div class="form-group">
            <label>Email</label>
            {{ form.render('email', ['class': 'form-control mb-2']) }}
            {{ form.messages('email') }}
        </div>
        
        <div class="form-group">
            <label>Password</label>
            {{ form.render('password', ['class': 'form-control mb-2']) }}
            {{ form.messages('password') }}
        </div>
        
        <div class="form-group">
            <label>Password Confirmation</label>
            {{ form.render('confirmPassword', ['class': 'form-control mb-2']) }}
            {{ form.messages('confirmPassword') }}
        </div>
        <div class="form-group">
            {{ form.render('Sign Up') }}
        </div>
        {{ form.render('csrf', ['value': security.getToken()]) }}
        {{ form.messages('csrf') }}
    </div>
</div>