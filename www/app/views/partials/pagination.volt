{% set url = router.getMatchedRoute().getName() %}
<ul class="pagination">
    {% if page.current != page.previous and page.previous %}
    <li class="page-item">
        <a class="page-link" href="/{{url}}?page={{ page.previous }}" aria-label="Previous">
            <span>Previous</span>
        </a>
    </li>
    {% endif %}

    {% if page.total_pages > 1 %}
        {% for index in 1..page.total_pages %}
        <li class="page-item {{ index === page.current ? 'disabled' : '' }}">
            <a class="page-link" href="/{{url}}?page={{index}}">{{ index }}</a>
        </li>
        {% endfor %}
    {% endif %}

    {% if page.next != page.current and page.next %}
    <li class="page-item">
        <a class="page-link" href="/{{url}}?page={{ page.next }}" aria-label="Next">
            <span aria-hidden="true">Next</span>
        </a>
    </li>
    {% endif %}
</ul>