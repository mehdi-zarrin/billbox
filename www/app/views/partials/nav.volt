<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Billbox</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    {% set currentRoute = router.getMatchedRoute().getName() %}

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            {% if auth.check() %}
                <li class="nav-item">
                    <a class="nav-link {{ currentRoute === null ? 'active' : '' }}" href="/">Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ currentRoute === 'orders' or currentRoute === 'deliveredOrders' ? 'active' : '' }}" href="/orders">My Orders</a>
                </li>

                <li class="nav-item">
                    <a href="/shopping-cart" class="nav-link {{ currentRoute === 'shoppingCart' ? 'active' : '' }}">Shopping Cart</a>
                </li>
            {% else %}
                <li class="nav-item">
                    <a class="nav-link" href="/sessions/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/sessions/register">Register</a>
                </li>
            {% endif %}
        </ul>

        <!-- Right Side Of Navbar -->
        {% if auth.check() %}
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ auth.get('name') }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/sessions/logout"
                       onclick="">
                        Logout
                    </a>
                </div>
            </li>
        </ul>
        {% endif %}
    </div>


</nav>