<?php

namespace Billbox\Services;
use Phalcon\Mvc\User\Component;

class Mail extends Component
{
    public function send($message, $to)
    {
        // just for the demonstration
        // in real world project I would use swift mailer to actually send the mail ;)
        $this->log->info('sending email with message: ' . $message . ' to:' . $to);
    }
}