<?php

namespace Billbox\Services;
use Phalcon\Mvc\User\Component;

class ShoppingCart extends Component
{
    public function add(array $item)
    {
        $contents = $this->getItems() ?: [];

        if(! in_array($item['id'], array_keys($contents))) {

            $contents[$item['id']]['item'] = $item;
            $contents[$item['id']]['qty'] = 1;

        } else {

            $contents[$item['id']]['qty'] = $contents[$item['id']]['qty'] + 1;
        }

        $contents[$item['id']]['total'] = $contents[$item['id']]['qty'] * $item['price'];

        $this->persistent->set('cart-contents', $contents);
    }

    public function getItems()
    {
        return $this->persistent->get('cart-contents');
    }

    public function clearCart()
    {
        $this->persistent->remove('cart-contents');
    }

    public function removeItem($id)
    {
        if ($contents = $this->persistent->get('cart-contents')) {
           if(isset($contents[$id])) {
               unset($contents[$id]);
               $this->persistent->set('cart-contents', $contents);
           }
        }
    }



}