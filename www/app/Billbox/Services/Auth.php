<?php
namespace Billbox\Services;
use Billbox\Exceptions\AuthenticationException;
use Phalcon\Mvc\User\Component;

class Auth extends Component
{
    public function attempt(array $credentials)
    {
        $user = $this->userRepository->findByEmail($credentials['email']);

        $message = 'Wrong username or password';
        if(! $user) {
            throw new AuthenticationException($message);
        }

        if(! $this->security->checkHash($credentials['password'], $user->password)) {
            throw new AuthenticationException($message);
        }

        $this->setSession($user);

    }

    public function loginWithId(int $id)
    {
        $user = $this->userRepository->findById($id);
        if($user) {
            $this->setSession($user);
            return true;
        }

        return false;
    }

    public function login($user)
    {
        if(!$user) return;
        $this->setSession($user);
    }


    public function check()
    {
        return $this->get() ?: false;
    }

    public function getUser()
    {
        if( $user = $this->check()) {

            if($user = $this->userRepository->findById($user['id'])) {
                return $user;
            }

        }

        return false;
    }

    public function get($field = null)
    {
        if($field){
            return $this->session->get('auth')[$field];
        }

        return $this->session->get('auth');
    }

    public function remove()
    {
        $this->session->remove('auth');
    }

    /**
     * @param $user
     */
    private function setSession($user)
    {
        $this->session->set('auth', [
            'id' => $user->id,
            'name' => (string)$user,
            'email' => $user->email
        ]);
    }


}