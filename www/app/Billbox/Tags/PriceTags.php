<?php
namespace Billbox\Tags;

use Phalcon\Tag;

class PriceTags extends Tag
{
    public static function formatPrice($value)
    {
        return number_format($value) . '$';
    }
}