<?php


namespace Billbox\Forms;


use Phalcon\Forms\Form;

abstract class AbstarctForm extends Form
{
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}