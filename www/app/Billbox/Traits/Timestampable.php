<?php
namespace Billbox\Traits;

use Carbon\Carbon;

trait Timestampable
{
    /**
     * @var int
     */
    protected $updatedAt;

    /**
     * @var int
     */
    protected $createdAt;

    public function beforeCreate()
    {
        $this->createdAt = time();
    }

    public function beforeUpdate()
    {
        $this->updatedAt = time();
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    public function getCreatedAt()
    {
        return (new Carbon($this->createdAt))->diffForHumans();
    }

    /**
    * @return bool|string
    * @throws \Exception
    */
    public function getUpdatedAt()
    {
        return (new Carbon($this->updatedAt))->diffForHumans();
    }
}