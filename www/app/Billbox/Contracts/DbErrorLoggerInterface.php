<?php
namespace Billbox\Contracts;

interface DbErrorLoggerInterface
{
    public function setMessages(array $messages);

    public function getMessages();

    public function log();


}