<?php
namespace Billbox\Middlewares;

use Phalcon\Di\Injectable;
use Sid\Phalcon\AuthMiddleware\MiddlewareInterface;

class MustBeLoggedIn extends Injectable implements MiddlewareInterface
{
    public function authenticate(): bool
    {
        if(!$this->auth->check()) {
            $this->response->redirect('sessions/login');
            return false;
        }

        return true;
    }
}