<?php
namespace Billbox\Behaviors;
use Phalcon\DiInterface;
use Phalcon\Di\InjectionAwareInterface;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

class UserRegistered extends Behavior implements BehaviorInterface
{
    protected $mail;
    protected $di;
    public function __construct($options = null, DiInterface $di)
    {
        parent::__construct($options);
        $this->mail = $di->get('mail');
    }

    public function notify($eventType, ModelInterface $model)
    {
        if($eventType == 'afterCreate') {
            // maybe send an email to the user !
            $this->mail->send('Welcome to out website', $model->email);
        }
    }

}