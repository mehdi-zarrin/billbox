<?php
namespace Billbox\Repositories;
use Phalcon\Di\Injectable;

abstract class AbstractDbRepository extends Injectable
{
    protected $messages;

    public function log()
    {
        $this->log->critical(print_r($this->getMessages(), true));
    }

    public function setMessages(array $message)
    {
        $this->messages = $message;
        $this->log();
    }

    public function getMessages()
    {
        return $this->messages;
    }

}