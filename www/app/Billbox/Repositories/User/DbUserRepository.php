<?php
namespace Billbox\Repositories\User;
use Billbox\Contracts\DbErrorLoggerInterface;
use Billbox\Exceptions\ValidationException;
use Billbox\Repositories\AbstractDbRepository;

class DbUserRepository extends AbstractDbRepository implements UserRepositoryInterface, DbErrorLoggerInterface
{
    public function findById(int $id)
    {
        return \Users::findFirstById($id);
    }

    public function findByEmail(string $email)
    {
        return \Users::findFirstByEmail($email);
    }

    public function save(array $attributes)
    {
        $users = new \Users($attributes);
        if(!$users->save()) {
            $this->setMessages($users->getMessages());
            throw new ValidationException('Could not insert into database');
        }

        return $users;
    }
}