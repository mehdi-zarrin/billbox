<?php
namespace Billbox\Repositories\User;

interface UserRepositoryInterface
{
    public function findById(int $id);

    public function findByEmail(string $email);

    public function save(array $attributes);

}