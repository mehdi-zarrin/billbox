<?php
namespace Billbox\Repositories\Order;

interface OrderRepositoryInterface
{
    public function findById(int $id);

    public function remove(int $id);

    public function saveWithItems(array $items, $user);
}