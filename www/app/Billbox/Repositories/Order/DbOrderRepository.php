<?php
namespace Billbox\Repositories\Order;

use Billbox\Contracts\DbErrorLoggerInterface;
use Billbox\Exceptions\ValidationException;
use Billbox\Repositories\AbstractDbRepository;
use OrderItems;
use Orders;

class DbOrderRepository extends AbstractDbRepository implements OrderRepositoryInterface, DbErrorLoggerInterface
{
    public function findById(int $id)
    {
        return Orders::findFirstById($id);
    }

    public function saveWithItems(array $items, $user)
    {
        $orderItems = [];
        $orderTotal = 0;
        foreach($items as $id => $item) {
            $orderItem = new OrderItems();
            $orderItem->productsId = $id;
            $orderItem->qty = $item['qty'];
            $total = $item['total'];
            $orderItem->total = $total;
            $orderTotal += $total;
            $orderItems[] = $orderItem;
        }
        $order = new Orders();
        $order->total = $orderTotal;
        $order->items = $orderItems;
        $user->orders = $order;

        if(! $user->save()) {
            $this->setMessages($user->getMessages());
            throw new ValidationException('Could not save to database');
        }
    }

    public function remove(int $id)
    {
        return Orders::findFirstById($id)->delete();
    }
}