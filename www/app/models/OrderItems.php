<?php


use Billbox\Helpers\Utils;
use Phalcon\Mvc\Model;

class OrderItems extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $productsId;

    /**
     * @var int
     */
    public $qty;

    /**
     * @var int
     */
    protected $total;

    /**
     * @var int
     */
    public $ordersId;

    public function initialize()
    {
        $this->belongsTo(
            'ordersId',
            'Orders',
            'id',
            [
                'alias' => 'order'
            ]
        );

        $this->belongsTo(
            'productsId',
            'Products',
            'id',
            [
                'alias' => 'product'
            ]
        );
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return \Billbox\Tags\PriceTags::formatPrice($this->total);
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }


}