<?php


use Billbox\Helpers\Utils;
use Phalcon\Mvc\Model;

class Products extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    protected $price;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return \Billbox\Tags\PriceTags::formatPrice($this->price);
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }


}