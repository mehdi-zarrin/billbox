<?php

use Billbox\Behaviors\UserRegistered;
use Phalcon\Mvc\Model;
use Billbox\Traits\Timestampable;


class Users extends Model
{
    use Timestampable;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    public function initialize()
    {
        $this->addBehavior(new UserRegistered(null, $this->getDI()));
        $this->hasMany(
            'id',
            'Orders',
            'usersId'
        );

        $this->hasMany(
            'id',
            'Orders',
            'usersId',
            [
                'alias' => 'latestOrders',
                'params' => [
                    'order' => 'createdAt desc'
                ]
            ]
        );

        $this->hasMany(
            'id',
            'Orders',
            'usersId',
            [
                'alias' => 'deliveredOrders',
                'params' => [
                    'conditions' => 'delivered = true'
                ]
            ]
        );
    }


    public function validation()
    {
        $validator = new Phalcon\Validation;
        $validator->add('email', new \Phalcon\Validation\Validator\Uniqueness([
            'message' => 'This email address has already been taken'
        ]));

        return $this->validate($validator);
    }


    public function __toString()
    {
        if($this->first_name && $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }

        return $this->email;
    }
}