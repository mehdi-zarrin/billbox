<?php

use Billbox\Helpers\Utils;
use Billbox\Traits\Timestampable;
use Phalcon\Mvc\Model;

class Orders extends Model
{
    use Timestampable;

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $usersId;

    public $ref;

    protected $total;

    /**
     * @var boolean
     */
    public $delivered = false;


    public function initialize()
    {
        $this->hasManyToMany(
            'id',
            'OrderItems',
            'ordersId', 'productsId',
            'Products',
            'id'
        );

        $this->belongsTo(
            'usersId',
            'Users',
            'id'
        );

        $this->hasMany(
            'id',
            'OrderItems',
            'ordersId',
            [
                'alias' => 'items'
            ]
        );
    }

    /**
     *  generates a random string for order reference number
     */
    public function beforeValidationOnCreate() {
        $this->generateRandomUniqueNumber();
    }


    /**
     * @return string
     */
    public function getTotal()
    {
        return \Billbox\Tags\PriceTags::formatPrice($this->total);
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }
    
    private function generateRandomUniqueNumber()
    {
        $this->ref = rand(1, 10000);
        $validator = new Phalcon\Validation;
        $validator->add('ref', new \Phalcon\Validation\Validator\Uniqueness);

        if(! $this->validate($validator)) {
            $this->generateRandomUniqueNumber();
        }
    }
    

}