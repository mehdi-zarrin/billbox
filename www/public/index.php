<?php
use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED);

(new Phalcon\Debug)->listen();
//ini_set('display_errors', 1);
//// convert warnings/notices to exceptions
//set_error_handler(function($errno, $errstr, $errfile, $errline) {
//    throw new \Exception($errstr.PHP_EOL.$errfile.":".$errline, $errno);
//});

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');



    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();


    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);



    echo $application->handle()->getContent();

