Note: I assume you already installed docker.

Please clone the project

then in project root run the following:

`docker-compose build && docker-compose up -d`

Then run:

`docker exec -i billbox_php bash -c "cd /var/www/site && composer install --no-interaction && vendor/bin/phalcon migration run"`

Then in your browser go to: 

`http://127.0.0.1:8080`

In case you want to see the database structure there is a adminer setup in below url: 

`http://127.0.0.1:8081`
